import java.util.*;
import java.util.stream.IntStream;

public class OddNPrime {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int number;
        int option;

        for (int i = 0; i < n; i++) {
            option = scanner.nextInt();
            number = scanner.nextInt();
            switch (option) {
                case 1:
                    System.out.println(isOdd(number) ? "ODD" : "EVEN");
                    break;
                case 2:
                    System.out.println(isPrime(number) ? "PRIME" : "COMPOSITE");
                    break;
                case 3:
                    System.out.println(isPalindrome(number) ? "PALINDROME" : "NOT A PALINDROME");
                    break;
            }
        }

    }

    private static boolean isPalindrome(int number) {

        return number == IntStream.iterate(number, n -> n != 0, i -> i / 10)
                .map(n -> n % 10)
                .reduce(0, (num, base) -> num * 10 + base);

    }

    private static boolean isPrime(int number) {

        return number > 1 && IntStream
                .range(2, number).noneMatch(result -> number % result == 0);

    }

    private static boolean isOdd(int number) {

        return number > 1 && IntStream
                .range(1, number).noneMatch(result -> number % 2 == 0);

    }

}
