import java.util.Arrays;
import java.util.Scanner;
import java.util.stream.IntStream;

public class Comma {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        String num = scanner.next();

        int[] numbers = Arrays.asList(num.split(","))
                .stream()
                .map(String::trim)
                .mapToInt(Integer::parseInt).toArray();

        String str = "";

        for (int i : numbers) {
            if (isOdd(i)) {
                str = str + "o" + i + ",";
            }else {
                str = str + "e" + i + ",";
            }
        }

        System.out.println(str.replaceAll(",$", ""));

    }

    private static boolean isOdd(int number) {

        return number > 1 && IntStream
                .range(1, number).noneMatch(result -> number % 2 == 0);

    }

}
