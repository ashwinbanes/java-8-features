import java.util.HashMap;
import java.util.Map;

public class Cards {

    enum Suits {

        CLUBS(1), DIAMONDS(2), HEARTS(3), SPADES(4);

        private int suitsVal;

        Suits(int val) {
            this.suitsVal = val;
        }

        public int getSuitsVal() {
            return suitsVal;
        }

    }

    enum Rank {

        ACE(1),
        TWO(2),
        THREE(3),
        FOUR(4),
        FIVE(5),
        SIX(6),
        SEVEN(7),
        EIGHT(8),
        NINE(9),
        TEN(10),
        JACK(11),
        QUEEN(12),
        KING(13);

        private int rankVal;

        Rank(int val) {
            this.rankVal = val;
        }

        public int getRankVal() {
            return rankVal;
        }
    }

    public static void main(String[] args) {

        Map<Integer, String> suits = new HashMap<>();

        suits.put(1, "Club");
        suits.put(2, "Diamonds");
        suits.put(3, "Hearts");
        suits.put(4, "Spades");

        Map<Integer, String> rank = new HashMap<>();

        rank.put(1, "Ace");
        rank.put(11, "Jack");
        rank.put(12, "Queen");
        rank.put(13, "King");


        for (int i = Suits.CLUBS.getSuitsVal(); i <= Suits.SPADES.getSuitsVal(); i++) {
            System.out.print(suits.get(i) + " : ");
            for (int j = Rank.ACE.getRankVal(); j <= Rank.KING.getRankVal(); j++) {
                System.out.print(rank.containsKey(j) ? rank.get(j) + " " : j + " ");
            }
            System.out.println();
        }

    }

}
