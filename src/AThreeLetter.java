import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class AThreeLetter {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        Predicate<String> filter = Pattern
                .compile("^a.[a-z]{1}$")
                .asPredicate();

        List<String> stringList = new ArrayList<>();

        int n = scanner.nextInt();
        String value;

        for (int i = 0; i < n; i++) {
            value = scanner.next();
            stringList.add(value);
        }

        List<String> filteredStrings = stringList
                .stream()
                .filter(filter)
                .collect(Collectors.toList());

        filteredStrings.forEach(System.out::println);

    }

}
