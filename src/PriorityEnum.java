public class PriorityEnum {

    enum Priority{

        HIGH(1), MEDIUM(2), LOW(3);

        private int priorityVal;

        Priority(int val){
            priorityVal = val;
        }

        public int getPriority(){
            return priorityVal;
        }
    }

    public static void main(String args[]){
        System.out.println("Higher Priority : " + Priority.HIGH.getPriority());
        System.out.println("Lower Priority : "+ Priority.LOW.getPriority());
        System.out.println("Medium Priority : "+ Priority.MEDIUM.getPriority());
    }
}
