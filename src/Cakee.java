public class Cakee implements Food {

    enum Cake {

        CAKE(2);

        private int cakeVal;

        Cake(int val) {
            this.cakeVal = val;
        }

        public int getCakeVal() {
            return cakeVal;
        }
    }

    @Override
    public int getType() {
        return Cake.CAKE.getCakeVal();
    }
}
