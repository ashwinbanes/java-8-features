import java.time.Month;
import java.time.YearMonth;
import java.util.Scanner;

public class LenOfMonth {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int year = scanner.nextInt();

        for (int i = 1; i <= 12; i++) {
            YearMonth yearMonth = YearMonth.of(year, i);
            int days = yearMonth.lengthOfMonth();
            System.out.println(Month.of(i) + " " + days);
        }

    }

}
