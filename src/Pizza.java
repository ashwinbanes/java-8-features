public class Pizza implements Food {

    enum Piz {

        PIZZA(1);

        private int pizVal;

        Piz(int val) {
            this.pizVal = val;
        }

        public int getPizVal() {
            return pizVal;
        }
    }

    @Override
    public int getType() {
        return Piz.PIZZA.getPizVal();
    }
}
